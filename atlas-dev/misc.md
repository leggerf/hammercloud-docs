# Miscellaneous

## Git branches
We develop in feature branch (name contains your username) and create merge request to ``master`` branch. 


## Coding style
We are following [PEP8](https://www.python.org/dev/peps/pep-0008/). In particular:

  * indentation by 4 spaces. no tabs.


## Logging
Use the ``self._logger`` object. e.g. 

   ```
self._logger.info('this is info message')
self._logger.debug('this is debug message')
self._logger.error('this is error message')
self._logger.critical('this is critical message')

   ```
Do not use ``print`` statements for logging. 

## Handling exceptions
Use ``try: ... except: ...`` statements. Use the ``traceback`` library. 
Leverage logging to help you trace and later reproduce issues with your code. 

## Locality of code execution
Some parts of the code of test lifetime are executed on the chosen submit node (the one listed on the test page, e.g. ``hammercloud-ai-21``), other parts can be executed on any node of the submit cluster ("distributed parts"). 

The "distributed parts" of the code are executed as [Celery](http://www.celeryproject.org/) tasks. 
A Celery task is a function in python. All tasks are listed in [apps/atlas/python/scripts/submit/tasks.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submit/tasks.py). 

Celery tasks are executed asynchronously, and return value is of type ``AsyncResult``, i.e. not the return value you expect. Therefore use Celery tasks only if the nature of the task allows you to not have to wait for the return value/result.

Celery tasks are called with `.delay(...)` method. The input parameter of the task are listed as input parameters of the `.delay()` method. 



