# Test generation

**Test generation:** from admin template and .tpl file the job configuration files (files jobs/*.py) are created.

## Test generation in ``celery_prod``
* Main script: [apps/atlas/python/scripts/submit/test_generate_celery.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submit/test_generate_celery.py)

  * main function: ``TestGenerateCelery.run()``
* Main wrapper: [/data/hc/scripts/submit/test-run.sh](https://gitlab.cern.ch/hammercloud/hammercloud-core/blob/master/hc_core/scripts/submit/test-run.sh#L56)

* Related scripts: 
  * ``from atlas.python.scripts.submit.configmaker import ConfigMaker`` [apps/atlas/python/scripts/submit/configmaker.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submit/configmaker.py) ... generate configuration, handle masks
  * ``from atlas.python.scripts.submit.jobconfig_register_sources import register_source`` [apps/atlas/python/scripts/submit/jobconfig_register_sources.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submit/jobconfig_register_sources.py) ... upload inputsandbox (tarballs)
  * ``from atlas.python.scripts.submit.tasks import register_buildlib`` [apps/atlas/python/scripts/submit/jobconfig_register_buildlib.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submit/jobconfig_register_buildlib.py) ...  upload build libs


## Test generation in ``celery_prod_jedi``
* Main script: [apps/atlas/python/scripts/submit/test_generate_celery_jedi.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submit/test_generate_celery_jedi.py)

  * main function: ``TestGenerateCeleryJedi.run()``
* Main wrapper: [/data/hc/scripts/submit/test-run.sh](https://gitlab.cern.ch/hammercloud/hammercloud-core/blob/master/hc_core/scripts/submit/test-run.sh#L74)

* Related scripts: 
  * ``from atlas.python.scripts.submit.test_generate_celery import TestGenerateCelery`` [apps/atlas/python/scripts/submit/test_generate_celery.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submit/test_generate_celery.py)
  * ``from atlas.python.scripts.submit.configmakerjedi import ConfigMakerJedi``  ... generate configuration, handle masks
from 
  * ``from atlas.python.scripts.submit.configmakerjedi import ConfigMakerJedi`` [apps/atlas/python/scripts/submit/configmakerjedi.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submit/configmakerjedi.py) ... generate configuration, handle masks
  * ``from atlas.python.scripts.submit.jobconfig_register_sources import register_source`` [apps/atlas/python/scripts/submit/jobconfig_register_sources.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submit/jobconfig_register_sources.py) ... upload inputsandbox (tarballs)
  * ``from atlas.python.scripts.submit.tasks import register_buildlib`` [apps/atlas/python/scripts/submit/jobconfig_register_buildlib.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submit/jobconfig_register_buildlib.py) ...  upload build libs
