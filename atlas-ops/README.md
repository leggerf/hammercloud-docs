# ATLAS ops
  * [Deploy Ganga patch](./deploy_ganga_patch.html)
  * [Check dataset cache](./check_ds_cache.html)
  * [Add/remove site to/from template](./add_site_to_template.html)